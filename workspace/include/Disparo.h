
// Disparo.h: interface for the Esfera class.
//
//////////////////////////////////////////////////////////////////////
//Autor MIguel JImenez
//#if !defined(AFX_DISPARO_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
//#define AFX_DISPARO_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_
#ifndef DISPARO_H_
#define DISPARO_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Disparo
{
public:
	Disparo();
	Disparo(int d, Vector2D pos);
	virtual ~Disparo();

	Vector2D centro;
	Vector2D velocidad;
	float radio;
	void Mueve(float t);
	void Dibuja();
};

#endif // !defined(AFX_DISPARO_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
